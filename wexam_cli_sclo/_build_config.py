# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import io
import sys
import csv
import pathlib
import argparse
import textwrap
import itertools

import cchardet
import yaml

DOC_YAML = textwrap.dedent(
    """
    Éditer ce fichier pour donner l'échelle de notation.

    Le nom de chaque exercice peut-être changé, mais le nom nom des questions NE
    DOIT PAS CHANGER.

    Pour chaque question, VOUS DEVEZ:
     - vérifier la valeur `raw`. Cette valeur correspond au nombre de points sur
       les cases de la copie. Cette valeur a été obtenue avec une heuristique
       utilisant entre autres le max dans le csv. Dans le cas d'une echelle par
       demi-point, il faut utiliser la même syntaxe que dans SCLO, c'est à dire
       "4,2" désigne une echelle de 4 par demi-points.
     - positionner dans `points` le nombre de points associé à la question.
    """
)


def _can_be_a_grade(x):
    try:
        a = int(x)
    except ValueError:
        return False
    if a == -10000:
        print(
            "The is a -10000 in the file. All incorrect value must be fixed before using this program.",
            file=sys.stderr,
        )
        sys.exit(1)
    if 0 <= a <= 1000:
        return True


def _infer_raw(raw_values):
    max_value = max(int(r) for r in raw_values)
    if max_value <= 1:
        return 1
    if max_value <= 2:
        return 2
    if max_value <= 4:
        return 4
    if max_value <= 5:
        return 5
    if max_value <= 8:
        return 8
    if max_value <= 10:
        return 10
    return max_value


def build_config(infilename, outfilename):
    csvcontent = open(infilename, "rb").read()
    encoding = cchardet.detect(csvcontent)["encoding"]
    if encoding is None:
        print("CSV encoding error", file=sys.stderr)
        sys.exit(1)
    csvdecoded = csvcontent.decode(encoding)
    dialect = csv.Sniffer().sniff(csvdecoded)
    csvreader = csv.DictReader(io.StringIO(csvdecoded), dialect=dialect)
    cols = csvreader.fieldnames
    data = list(csvreader)

    questions = tuple(
        c
        for i, c in enumerate(cols)
        if i > 0 and "." in c and all(_can_be_a_grade(d[c]) for d in data)
    )

    exos = [
        {
            "name": exo,
            "questions": {
                q: {"raw": _infer_raw(d[q] for d in data), "points": 1}
                for q in sorted(quests, key=lambda x: int(x.split(".")[-1]))
            },
        }
        for exo, quests in itertools.groupby(
            questions, key=lambda x: ".".join(x.split(".")[:-1])
        )
    ]

    config = {"exam_name": "Hiver 1931 - ZZ02 - Examen médian", "config": exos}

    config_yaml = yaml.safe_dump(
        config, indent=4, sort_keys=False, allow_unicode=True
    ).replace("\n-", "\n\n-")

    with open(outfilename, "w") as f:
        f.write(textwrap.indent(DOC_YAML, "# "))
        f.write("\n\n\n")
        f.write(config_yaml)


def main():
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(
            """
            Generate editable scale file from AMC csv export.
            """
        ),
    )

    parser.add_argument(
        "--version", action="version", version="", help=argparse.SUPPRESS
    )

    parser.add_argument(
        "-o",
        "--output",
        help="""
                Output file. If the output file is not provided, the output
                file will be the same as the input file with the '.yml' extension.
                """,
        default=None,
    )

    parser.add_argument(
        "input_csv",
        help="""
                Input csv file from AMC export.
            """,
    )

    args = parser.parse_args()

    if args.output is not None:
        fout = args.output
    else:
        fout = pathlib.Path(args.input_csv).with_suffix(".yml")

    build_config(args.input_csv, fout)


if __name__ == "__main__":
    main()
