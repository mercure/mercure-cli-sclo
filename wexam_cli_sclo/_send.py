# Copyright 2022, Thibaud Le Graverend <thibaud@legraverend.fr>
# Copyright 2022, Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import re
import io
import sys
import csv
import json
import argparse
import textwrap
import pathlib
import itertools
import multiprocessing
from getpass import getpass
from dataclasses import dataclass

import yaml
import cchardet
import requests
import mechanize
import numpy as np
from PIL import Image, ImageDraw, ImageFont

import wexam_pdfimg


@dataclass
class StudentRes:
    login: str
    paper: int
    questions: dict


@dataclass
class StudentPaperDesc:
    login: str
    paper_pdf: pathlib.Path
    grade: float
    abstracts: list


@dataclass
class StudentPaper:
    login: str
    grade: float
    pages: list


def _ech(x):
    try:
        y = int(x)
        return y, 1
    except ValueError:
        pass
    return tuple(int(w) for w in x.split(","))


def _ech_max(x):
    up, step = _ech(x)
    return up * step


def _all_scale(x):
    up, step = _ech(x)
    return tuple(
        (i, (r // step if r % step == 0 else r / step))
        for i, r in enumerate(range(0, step * up + 1))
    )


def compute_grades_stats(config, data):
    hists = {
        q: {
            val: sum(1 for d in data if d.questions[q] == k)
            for k, val in _all_scale(qconfig["raw"])
        }
        for q, qconfig in itertools.chain.from_iterable(
            e["questions"].items() for e in config["config"]
        )
    }

    grades = {
        stud.login: [
            sum(
                stud.questions[q] / _ech_max(qconfig["raw"]) * qconfig["points"]
                for q, qconfig in exercise["questions"].items()
            )
            for exercise in config["config"]
        ]
        for stud in data
    }

    for val in grades.values():
        val.append(sum(val))

    quantiles = tuple(
        tuple(x)
        for x in np.quantile(
            np.array(list(grades.values())), (0, 0.25, 0.5, 0.75, 1), axis=0
        ).T
    )

    return grades, hists, quantiles


def sflo(x):
    if x % 1 == 0:
        return int(x)
    return x


def build_papersdesc(config, data, pdfs):
    grades, hists, quantiles = compute_grades_stats(config, data)

    papers = []
    for stud in data:

        abstracts = []

        stud_grades = grades[stud.login]
        for exercise, exercise_grade, exercise_quantiles in zip(
            config["config"], stud_grades, quantiles
        ):
            txt = io.StringIO()
            print(config["exam_name"], file=txt)
            print("=" * len(config["exam_name"]), file=txt)
            print("\n\n", file=txt)

            subname = f"Exercice « {exercise['name']} »"
            print(subname, file=txt)
            print("-" * len(subname), file=txt)
            print("\n", file=txt)

            for i, (q, qconfig) in enumerate(exercise["questions"].items()):
                up, step = _ech(qconfig["raw"])
                print(f"  - Question {i+1}", file=txt)
                print(
                    f"        Score obtenu par l'étudiant : {sflo(stud.questions[q]/step)}/{sflo(up)}",
                    file=txt,
                )
                print(f"        Score de la promotion : {hists[q]}", file=txt)
                print(f"        Barème : {qconfig['points']} pts", file=txt)
                print(
                    f"        Points obtenus par l'étudiant : {stud.questions[q]/(up*step)*qconfig['points']} pts",
                    file=txt,
                )
            print(file=txt)
            print(
                f"Total de l'exercice obtenu par l'étudiant : {exercise_grade} pts",
                file=txt,
            )
            print(
                f"Total de l'exercice obtenu par la promotion :\n"
                f"    (min, q1, q2, q3, max) = {exercise_quantiles}",
                file=txt,
            )
            abstracts.append(txt.getvalue())

        txt = io.StringIO()
        print(config["exam_name"], file=txt)
        print("=" * len(config["exam_name"]), file=txt)
        print("\n\n", file=txt)

        subname = "Total"
        print(subname, file=txt)
        print("-" * len(subname), file=txt)
        print("\n", file=txt)

        print(f"Total obtenu par l'étudiant : {stud_grades[-1]} pts", file=txt)
        print(
            f"Total obtenu par la promotion :\n"
            f"    (min, q1, q2, q3, max) = {quantiles[-1]}",
            file=txt,
        )
        abstracts.append(txt.getvalue())

        papers.append(
            StudentPaperDesc(
                login=stud.login,
                abstracts=abstracts,
                grade=stud_grades[-1],
                paper_pdf=pdfs[stud.paper],
            )
        )

    return papers


def abstract_to_png(abstract):
    out = Image.new("RGB", (2480 // 2, 3508 // 2), (255, 255, 255))
    fnt = ImageFont.truetype("Pillow/Tests/fonts/FreeMono.ttf", 20)
    d = ImageDraw.Draw(out)
    d.multiline_text((40, 40), abstract, font=fnt, fill=(0, 0, 0))

    out = out.convert("P", palette=Image.Palette.ADAPTIVE, colors=256)
    stream = io.BytesIO()
    out.save(stream, format="png", optimize=True)
    return stream.getvalue()


def build_paper(paperdesc: StudentPaperDesc) -> StudentPaper:
    ret = StudentPaper(login=paperdesc.login, grade=paperdesc.grade, pages=[])
    for p in wexam_pdfimg.RawPdf(open(paperdesc.paper_pdf, "rb").read()):
        ret.pages.append(p.content)

    for abstract in paperdesc.abstracts:
        ret.pages.append(abstract_to_png(abstract))

    return ret


def main():
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(
            """
            Send associated file with grade to wexam app
            """
        ),
    )

    parser.add_argument(
        "-u",
        "--url",
        help="Url of the exam manage page. If not provided, the url is asked.",
        default=None,
    )

    parser.add_argument(
        "-l",
        "--login",
        help="""
                Login used for authentification. If not provided, the login is
                asked. If the environment var WEXAM_AUTH_PASSWD is set, the
                value is used for auth, otherwise the passwed is asked.
                """,
        default=None,
    )

    parser.add_argument("-q", "--quiet", help="Quiet mode.", action="store_true")

    parser.add_argument(
        "config_yaml",
        help="Exam configuration and grading scale.",
    )

    parser.add_argument(
        "exam_csv",
        help="Exam csv export from AMC.",
    )

    parser.add_argument(
        "pdf_dir",
        metavar="pdf_dir/",
        help="Directory which contains exported pdf from AMC.",
    )

    args = parser.parse_args()

    config = yaml.safe_load(open(args.config_yaml))
    questions = tuple(
        itertools.chain.from_iterable(x["questions"] for x in config["config"])
    )

    csvcontent = open(args.exam_csv, "rb").read()
    encoding = cchardet.detect(csvcontent)["encoding"]
    if encoding is None:
        print("CSV encoding error", file=sys.stderr)
        sys.exit(1)
    csvdecoded = csvcontent.decode(encoding)
    dialect = csv.Sniffer().sniff(csvdecoded)
    csvreader = csv.DictReader(io.StringIO(csvdecoded), dialect=dialect)
    cols = csvreader.fieldnames
    logincols = [c for c in cols if "login" in c.lower()]
    if not logincols:
        print("csvfile: Login column not found", file=sys.stderr)
        sys.exit(1)
    if len(logincols) > 1:
        print("csvfile: Login column is ambiguous", file=sys.stderr)
        sys.exit(1)
    logincol = logincols[0]
    papercol = cols[0]
    if not all(c in cols for c in questions):
        print(
            f"Some columns in config are not in csv file: {tuple(c for c in questions if c not in cols)!r}",
            file=sys.stderr,
        )
        sys.exit()
    data = [
        StudentRes(
            paper=int(row[papercol]),
            login=row[logincol],
            questions={q: int(row[q]) for q in questions},
        )
        for row in csvreader
    ]

    pdf_dir = pathlib.Path(args.pdf_dir)
    pdfs = {
        int(x.name.split("-")[0].split(".")[0]): x
        for x in pdf_dir.iterdir()
        if x.is_file() and x.suffix == ".pdf"
    }
    if not pdfs:
        print("No pdf found in directory", file=sys.stderr)
        sys.exit(1)

    not_found_pdf = tuple((s.paper, s.login) for s in data if s.paper not in pdfs)
    if not_found_pdf:
        print(f"Following pdf are not found: {not_found_pdf!r}.", file=sys.stderr)
        sys.exit(1)

    if args.url is None:
        url = input("Url of the exam manage page: ")
    else:
        url = args.url

    if not url.endswith("/manage"):
        print("The url must end by '/manage'.", file=sys.stderr)
        sys.exit(1)

    if args.login is None:
        login = input("Login: ")
    else:
        login = args.login

    passwd = os.getenv("WEXAM_AUTH_PASSWD")
    if passwd is None:
        passwd = getpass("Password: ", stream=sys.stderr)

    if args.quiet:

        def status(x):
            pass

    else:

        def status(x):
            print(x, file=sys.stderr)

    papersdesc = build_papersdesc(config, data, pdfs)

    with multiprocessing.Pool(
        processes=(1 if os.cpu_count() < 2 else os.cpu_count() - 1)
    ) as pool:
        sendable_papers = pool.imap_unordered(build_paper, papersdesc)

        cookies = get_cookies(url, login, passwd)
        upload_url = get_upload_url(url, cookies)
        requests_session = requests.Session()
        for paper in sendable_papers:
            send_paper(requests_session, cookies, upload_url, paper, status)


def send_paper(requests_session, cookies, upload_url, paper, status):
    data = {
        "login": paper.login,
        "grade": paper.grade,
    }

    files = [
        (
            "page",
            ("page", page, "image/png" if page[:4] == b"\x89PNG" else "image/jpeg"),
        )
        for page in paper.pages
    ]
    status(
        f"Send paper for login={paper.login!r}, grade={paper.grade!r}, pages={len(paper.pages)}, paper_size={sum(len(p) for p in paper.pages)/2**20:.1f}MiB"
    )
    rep = requests_session.post(upload_url, data=data, files=files, cookies=cookies)
    if not rep.ok:
        print("Unknown error", file=sys.stderr)
        sys.exit(1)
    objrep = json.loads(rep.text)
    status(f' → {objrep["status"]}, {objrep["msg"]}')


def get_upload_url(url, cookies):
    baseurl = url.split("/")[:-5]
    rep = requests.get(url, cookies=cookies)
    a = re.match(
        '.*?action="/exam/upload/([0-9]+)"',
        "".join(x.strip() for x in rep.text.splitlines()),
    )
    if not a:
        print("Upload part not found.", file=sys.stderr)
        sys.exit(1)
    (partid,) = a.groups()
    return "/".join(baseurl + ["exam", "rawupload", partid])


def get_cookies(url, login, passwd):
    cookies = mechanize.CookieJar()
    br = mechanize.Browser()
    br.set_cookiejar(cookies)
    br.set_handle_robots(False)
    br.open(url)
    form = br.forms()[0]
    form["username"] = login
    form["password"] = passwd
    req = form.click()
    try:
        rep = br.open(req)
    except:
        print("Login error.", file=sys.stderr)
        sys.exit(1)
    if rep is None or rep.geturl() != url:
        print("Login/password error.", file=sys.stderr)
        sys.exit(1)

    return cookies


if __name__ == "__main__":
    main()
