# Wexam cli sclo

CLI tool to send already associated papers obtained with SCLO and AMC.

## Installation

## External dependencies

`pdfinfo` and `pdftoppm` must be in the `PATH`. These are provided by
`poppler-utils`.

```bash
apt install poppler-utils
```

### Production version

```bash
pip install --upgrade wexam-cli-sclo --index-url https://gitlab.utc.fr/api/v4/groups/12819/-/packages/pypi/simple
```

# Basic usage

## Generate the file for grading scale, a example with a existing exam:

```bash
cd ~/Projets-QCM/A2021_SY02_median/
wexam-cli-sclo-generate-conf -o bareme.yml export/A2021_SY02_median.csv
```

Edit the yaml file to enter the scale, the exam name, and exercises names.

And upload:

```bash
wexam-cli-sclo-upload -u https://wexam.utc.fr/exam/A21/median/manage bareme.yml export/A2021_SY02_median.csv cr/corrections/pdf/
```

Where:
 - `-u https://wexam.utc.fr/exam/A21/median/manage` is the manage page of the
   exam (the exam must be created before upload).
 - `bareme.yml` is the scale and other config infos. See previous step.
 - `export/A2021_SY02_median.csv` the exported results in AMC.
 - `cr/corrections/pdf/` the directory which contains pdf exported by AMC.
