from setuptools import find_packages, setup
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "wexam_cli_sclo", "__init__.py"), encoding="utf-8") as f:
    ast = compile(f.read(), "__init__.py", "exec")
    fake_global = {"__name__": "__main__"}
    try:
        exec(ast, fake_global)
    except (SystemError, ImportError) as e:
        print("System error")

    version = fake_global["__version__"]

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = "".join(f.readlines())


with open(path.join(here, "requirements.txt"), encoding="utf-8") as f:
    requirements = [x.strip() for x in f.readlines()]

with open(path.join(here, "requirements-dev.txt"), encoding="utf-8") as f:
    requirements_dev = [x.strip() for x in f.readlines()]

setup(
    name="wexam-cli-sclo",
    version=version,
    description="CLI to upload papers to wexam app",
    packages=find_packages(),
    long_description_content_type="text/markdown",
    long_description=long_description,
    url="https://gitlab.com/wexam/wexam-cli-sclo",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
    ],
    package_data={},
    zip_safe=False,
    install_requires=requirements,
    python_requires=">=3.8",
    entry_points={
        "console_scripts": [
            "wexam-cli-sclo-generate-conf=wexam._build_config:main",
            "wexam-cli-sclo-upload=wexam._send:main",
        ],
    },
)
